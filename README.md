# Update Notifier for Atlassian apps  ![alt text](src/main/resources/atlassian/updatenotifier/images/plugin-logo.png "")

Never miss an Atlassian application update again. This plugins adds a short notification on top of your Atlassian app,
if there is a newer version. It's only shown if you are an admin.
![alt text](psd/Banner.png "")

* Atlassian Marketplace: https://marketplace.atlassian.com/plugins/confluence.updatenotifier