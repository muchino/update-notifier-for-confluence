require(['aui/flag', 'jquery', 'ajs'], function (flag, $, ajs) {

  var STORAGE_KEY = "hideAtlassianAppVersion";

  $.getJSON(ajs.contextPath() + "/rest/versioncheck/1.0/", function (data) {

    if (data.hasOwnProperty("title") &&
            data.hasOwnProperty("version") &&
            data.version.hasOwnProperty("version") &&
            data.hasOwnProperty("message")) {

      var version = data.version.version;
      var storedVersion = localStorage.getItem(STORAGE_KEY);

      if (storedVersion !== version) {
        var versionInfo = flag({
          type: 'info',
          title: data.title,
          persistent: false,
          body: data.message
        });
        $('.updatenotifier-version-dismiss').click(function (e) {
          e.preventDefault();
          localStorage.setItem(STORAGE_KEY, version);
          versionInfo.close();
        })
      }
    }
  });
});