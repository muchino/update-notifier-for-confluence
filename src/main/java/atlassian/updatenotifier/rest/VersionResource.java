package atlassian.updatenotifier.rest;

import atlassian.updatenotifier.model.VersionModel;
import atlassian.updatenotifier.providers.VersionProvider;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.permission.PermissionEnforcer;
import java.util.HashMap;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;


@Path("/")
public class VersionResource {

	private final VersionProvider versionProvider;
	private final I18nResolver i18nResolver;
	private final ApplicationProperties applicationProperties;
	private final PermissionEnforcer permissionEnforcer;


	@Autowired
	public VersionResource(VersionProvider versionProvider,
						   @ComponentImport I18nResolver i18nResolver,
						   @ComponentImport ApplicationProperties applicationProperties,
						   @ComponentImport PermissionEnforcer permissionEnforcer) {
		this.versionProvider = versionProvider;
		this.i18nResolver = i18nResolver;
		this.applicationProperties = applicationProperties;
		this.permissionEnforcer = permissionEnforcer;
	}

	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response getVersion() {

		HashMap<String, Object> returnMap = new HashMap<>();

		if (permissionEnforcer.isAdmin()) {

			if (versionProvider.isNewVersionAvailable()) {

				VersionModel version = versionProvider.getNewestVersion();

				StringBuilder message = new StringBuilder(i18nResolver.getText("atlassian.updatenotifier.message",
					getApplicationName()));

				message.append("<div class='updatenotifier-wrapper'>");
				// add release notes
				if (StringUtils.isNotBlank(version.getReleaseNotesURL())) {
					message.append("<a target='_blank' href='");
					message.append(version.getReleaseNotesURL());
					message.append("'>");
					message.append(i18nResolver.getText("atlassian.updatenotifier.releaseNotes"));
					message.append("</a>&nbsp;&middot;&nbsp;");
				}

				// add release notes
				if (StringUtils.isNotBlank(version.getUpgradeNotes())) {
					message.append("<a target='_blank' href='");
					message.append(version.getUpgradeNotes());
					message.append("'>");
					message.append(i18nResolver.getText("atlassian.updatenotifier.upgradeNotes"));
					message.append("</a>&nbsp;&middot;&nbsp;");
				}

				message.append("<a  href='' class='updatenotifier-version-dismiss'>");
				message.append(i18nResolver.getText("atlassian.updatenotifier.hide"));

				returnMap.put("version", version);
				returnMap.put("title", i18nResolver.getText("atlassian.updatenotifier.title",
					getApplicationName(),
					version.getVersion()));
				returnMap.put("message", message.toString());

			} else {
				returnMap.put("message", "No newer version found");

			}

		} else {
			returnMap.put("error", "You are not an admin");
		}

		return Response.ok(returnMap).build();
	}

	private String getApplicationName() {
		return StringUtils.capitalize(applicationProperties.getDisplayName().toLowerCase());
	}
}