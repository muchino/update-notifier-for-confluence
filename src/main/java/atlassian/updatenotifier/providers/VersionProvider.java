package atlassian.updatenotifier.providers;

import atlassian.updatenotifier.model.VersionModel;
import com.atlassian.json.jsonorg.JSONObject;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.ApplicationProperties;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VersionProvider {


	private static final String BASE_URL = "https://my.atlassian.com/download/feeds/current/";

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final VersionModel currentVersion;
	private final Minutes minInterval = Minutes.minutes(240);
	private final ApplicationProperties applicationProperties;
	private String url;
	private DateTime lastCheck = null;
	private VersionModel onlineVersion = null;

	@Autowired
	public VersionProvider(@ComponentImport ApplicationProperties applicationProperties) {
		this.applicationProperties = applicationProperties;
		this.currentVersion = new VersionModel(applicationProperties.getVersion(), getApplication());
		this.url = BASE_URL + getApplication() + ".json";
	}

	public VersionModel getNewestVersion() {
		if (this.isCheckNecessary()) {
			this.lastCheck = new DateTime();
			try {
				StringBuilder sb;
				String line;
				HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
				conn.setConnectTimeout(2000); //set timeout to 2 seconds
				conn.setRequestMethod("GET");
				conn.setDoOutput(true);
				if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
					conn.connect();
					InputStreamReader in = new InputStreamReader((InputStream) conn.getContent());
					BufferedReader buff = new BufferedReader(in);
					sb = new StringBuilder();
					while ((line = buff.readLine()) != null) {
						sb.append(line);
					}
					if (sb.length() > 11) {
						JSONObject jsonObject = new JSONObject(sb.subSequence(11, sb.length() - 2).toString());
						String version = (String) jsonObject.get("version");
						this.onlineVersion = new VersionModel(version, getApplication());
						this.onlineVersion.setReleaseNotesURL((String) jsonObject.get("releaseNotes"));
						this.onlineVersion.setUpgradeNotes((String) jsonObject.get("upgradeNotes"));
					}
				}
			} catch (Exception e) {
				logger.warn("Error while retrieving the remote Confluence version ", e);
			}
			if (this.onlineVersion == null) {
				this.onlineVersion = currentVersion;
			}
		}
		return this.onlineVersion;
	}

	private boolean isCheckNecessary() {
		if (lastCheck == null || onlineVersion == null) {
			return true;
		}

		Minutes interval = Minutes.minutesBetween(lastCheck, new DateTime());
		return interval.isGreaterThan(minInterval);
	}

	private String getApplication() {

		String application = applicationProperties.getPlatformId();
		switch (application) {
			case "jira":
				return "jira-core";
			case "bitbucket":
				return "stash";
			case "conf":
				return "confluence";
			default:
				return application;
		}
	}

	public boolean isNewVersionAvailable() {
		return currentVersion.isOlderThan(getNewestVersion());
	}
}
